/*
****TesJo
****Taller de Sistemas Operativos 
****Alumno: Gustavo Pérez Barrios
****IC-602
****
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<arpa/inet.h>


#define PORT 5550 // cambiar para que se introdusca el puerto

void main(){
    int clienteSocket;
    struct sockaddr_in serverAddr;
    char buffer[1024];

    clienteSocket=socket(PF_INET,SOCK_STREAM,0);
    printf("\n->Socket de cliente creado correctamente");
    memset(&serverAddr,'\0',sizeof(serverAddr));
    serverAddr.sin_family=AF_INET;
    serverAddr.sin_port=htons(PORT);
    serverAddr.sin_addr.s_addr=inet_addr("127.0.0.1"); //cambiar para introducir la ip

    connect(clienteSocket,(struct sockaddr*)&serverAddr,sizeof(serverAddr));
    printf("\n->Conectando con el servidor");
    recv(clienteSocket, buffer,1024,0);
    printf("\n->Datos Recibidos: %s ",buffer);
    printf("\n");
    


}
