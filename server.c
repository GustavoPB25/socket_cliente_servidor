/*
****TesJo
****Taller de Sistemas Operativos 
****Alumno: Gustavo P�rez Barrios
****IC-602
****
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<arpa/inet.h>


#define PORT 5550

void main(){
    int sockfd;
    struct sockaddr_in serverAddr;
    
    int newSocket;
    struct sockaddr_in newAddr;

    socklen_t addr_size;
    char buffer[1024];

    sockfd=socket(PF_INET, SOCK_STREAM, 0);
    printf("\n->Socket de servidor creado correctamente");
    
        memset(&serverAddr,'\0', sizeof(serverAddr));
        serverAddr.sin_family=AF_INET;
        serverAddr.sin_port=htons(PORT);
        serverAddr.sin_addr.s_addr=inet_addr("127.0.0.1");
        if(bind(sockfd,(struct sockaddr*)&serverAddr, sizeof(serverAddr))== -1){
            printf("Error el puerto %d esta sienddo usado",PORT);
        }else{

            bind(sockfd,(struct sockaddr*)&serverAddr, sizeof(serverAddr));
            printf("\n->vincularse al n�mero de puerto %d",PORT);
            listen(sockfd,5);
            printf("\n->Escuchando");
            addr_size=sizeof(newAddr);
            newSocket=accept(sockfd,(struct sockaddr*)&newAddr,&addr_size);

            char msg[1024];
            printf("\n->Teclea el mansaje a enviar:");
            scanf("%s",msg);


            strcpy(buffer,msg); //cambiar para poner el mensaje
            send(newSocket,buffer,strlen(buffer),0);    
        
            printf("\n-->Mensaje enviado\n");
        }
}